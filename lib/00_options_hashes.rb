# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

def transmogrify(str_input, options = {})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false
  }
  ans = str_input
  options = defaults.merge(options)
  #number of times repeated
  (2..options[:times]).each{|num| ans += str_input }
  #if upcase
  if options[:upcase]
    ans = ans.upcase
  end
  #if reverse
  if options[:reverse]
    ans = ans.reverse
  end
  ans
end
